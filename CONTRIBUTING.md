# Developing AUI

## Requirements

Building and working with AUI's source requires at minimum:

- Node 6+
- yarn 1.1+
- Java 1.7+ - for building the soy templates.

AUI has optional dependencies for building parts of it:

- Maven 3.2.5 - for building the P2 plugin and its test harnesses.
- docker-machine 0.13+ - for running various tests in a container.

## Development environment

First, install all the required dependencies:

    yarn install

### CLI

To see a list of the commands:

    yarn

To see the help for a specific command pass `--help` to it.

### Git hooks

To set up git hooks run `yarn setuphooks`.

## Building

You can then build any of the various production assets of AUI:

    yarn dist            // builds AUI as a set of concatenated files
                         // appropriate for using in a browser.

    yarn p2-plugin       // compiles the AUI source as an Atlassian P2 Plugin
                         // NOTE: requires Maven to be available

    yarn docs            // builds the AUI documentation.

## Testing

Although we encourage you write your tests first, then your code, you can test your changes in a couple areas.

### Unit tests

We use [Karma](https://karma-runner.github.io) for running our tests.

To run tests once:

    yarn test

To run the tests in watch mode:

    yarn test/watch

Both scripts accept the same arguments:

* `--browsers [Chrome,Firefox]` --
  The browsers to run the tests in.
  Valid values are `Chrome`, `Firefox`, `Opera` and `Safari`.

* `--grep [pattern]` --
  Specify a pattern to match a subset of tests to run.

*It's recommended to just run a single describe block in TDD mode, use [`describe.only`](https://mochajs.org/#exclusive-tests)*

### Unit tests via Docker

Alternatively, you can run the Chrome and Firefox unit tests via `yarn test-docker/chrome` or `yarn test-docker/firefox`.

### Acceptance testing

We have a set of test pages that help stress-test the AUI components and patterns during development and acceptance testing.
There are two ways to run the reference application:

#### A static site (aka the "flatapp")

Running AUI and its test pages as a set of static HTML, CSS, and JavaScript files is the fastest way to use them.

To run the application in a production-like environment, use:

    yarn flatapp/run

By default, this will open up a page at [http://0.0.0.0:7000/pages/](http://0.0.0.0:7000/pages/).
You can configure where the server will run:

* `--host [0.0.0.0]` -- The host to start the server on.
* `--port [7000]` -- The port to start the server on.

#### An Atlassian P2 plugin

The test pages can be run inside the Atlassian reference application plugin environment (aka the "refapp").
This is slower, but the closest analog to how AUI is consumed by Atlassian products.

To run the application in a production-like environment, use:

    yarn p2-plugin/run

By default, this will run the Atlassian plugin at [http://0.0.0.0:9999/ajs/](http://0.0.0.0:9999/ajs/).

### Visual regression testing

We have a set of npm script dedicated to run on CI (Bamboo). They are prefixed with `visreg` and you do not have to run
them manually. If you need to add more of those and check if they work you can run: `yarn visreg/flatapp/docker/chrome` 
(which runs on Docker) or `yarn visreg/flatapp/ci` if it is fine for you to run it locally. 

We do not commit resulting artifacts to the repo - the CI job does that by creating new branch on 
`https://bitbucket.org/atlassian/aui-visual-regression` (Only in case of new screenshots generated). Just remember to
make a PR out of it.

## Documenting

All the documentation on [https://aui.atlassian.com] is developed and released from this repository.

To run the application in a production-like environment, use:

    yarn docs/run

By default this will open up a page at [http://0.0.0.0:8000/](http://0.0.0.0:8000/).
You can configure where the server will run:

* `--host [0.0.0.0]` -- The host to start the server on.
* `--port [8000]` -- The port to start the server on.

## Developing

The documentation and reference applications can all be built and watched for changes:

    yarn docs/watch
    yarn flatapp/watch
    yarn refapp/watch

Each accepts the same parameters as their `*/run` counterparts. In watch mode, files will be built via Webpack,
which is significantly faster than the previous build process.

Future versions of AUI will refactor and update the production builds to use Webpack too, but for now,
Webpack is only used during development.

### Css toggles
We use `.aui-legacy-x` for all our CSS feature flag toggles.

## Contributing

When contributing to AUI, we ask that you follow these steps.

1. Create an issue in the [AUI project](https://ecosystem.atlassian.net/browse/AUI).

   Creating an issue is a good place to talk with the AUI team about whether anyone else is
   working on the same issue, what the best fix is, and if this is a new feature, whether it belongs in AUI.

   If you don't create an issue, we'll ask you to create one when you issue the PR and re-tag your
   commits with the issue key.

2. Create a branch for your change.

    a. If your change is meant for a specific version of AUI, start your branch against the appropriate
       version branch of AUI. For example, if you are attempting to fix a bug in AUI 6.0.8, start your
       branch from the `6.0.x` branch of AUI.

    b. Name your branch in the format `{issue-key}-{description}` -- for example,
       `AUI-1337-fix-the-contributor-guide`.

3. Make your changes.

    a. Write tests. Unit tests are preferred over integration tests.

    b. Ensure all your commits are tagged with the issue key, ideally at the start of the commit message --
       for example, `AUI-1337 - fixes to contributor guide`.

4. Push your branch and create a pull request.

    a. You should fork the AUI repository, and push your branch to your fork.

    b. When creating your pull-request, target the earliest branch of AUI you expect the fix to go in to.
       For example, if you were fixing a bug in AUI 6.0.8 and expect a new release of AUI 6.0.x, target `6.0.x`.
       If, your change is a new feature or is a significant backwards-incompatible change, target `master`.
