/// <reference types="Cypress" />
/* eslint-disable */

const variants = {
    'docs/typography.html': ['.aui-page-panel-nav', '.aui-page-panel-content'],
    'docs/app-header.html': ['.aui-flatpack-example'],
    'docs/banner-example-iframe.html': ['.aui-flatpack-example'],
    'docs/forms.html': ['.aui-flatpack-example'],
    'docs/labels.html': ['.aui-flatpack-example'],
    'docs/lozenges.html': ['.aui-flatpack-example'],
    'docs/messages.html': ['.aui-flatpack-example'],
    'docs/navigation.html': [
        '#horizontal-nav-example',
        '#vertical-nav-example',
        '#pagination-example',
        '#actions-list-example',
    ],
    'docs/page-header.html': ['#pageheader-example'],
    'docs/progress-tracker.html': [
        '#default-progress-tracker-example',
        '#inverted-progress-tracker-example'
    ],
    'docs/tabs.html': ['.aui-flatpack-example'],
    'docs/toolbar2.html': ['.aui-flatpack-example'],
    'docs/dialog2.html': ['#static-dialog-example'],
    'docs/progress-indicator.html': ['#progress-example-attrs-and-props']
};

Object.entries(variants).forEach(([path, selectors]) => {
    context(path, () => {
        const pathPrefix = path.replace(/\//g, '_');

        beforeEach(() => {
            cy.visit(path);
        });

        selectors.forEach(selector => {
            it(selector, () => {
                cy.get(selector).matchImageSnapshot(`${pathPrefix}_${selector}`)
            })
        });
    });
});

it('docs/icons.html', () => {
    const path = 'docs/icons.html';
    const pathPrefix = path.replace(/\//g, '_');
    const selector = '#icons-list';
    cy.visit(path);
    cy.get('#icons-list .aui-icon-small');
    cy.get(selector).first().matchImageSnapshot(`${pathPrefix}_${selector}`)
});

it('docs/toggle-button.html', () => {
    const path = 'docs/toggle-button.html';
    const pathPrefix = path.replace(/\//g, '_');
    const selector = 'aui-docs-example';
    cy.visit(path);
    cy.get(selector).first().matchImageSnapshot(`${pathPrefix}_${selector}`)
});

it('docs/dropdown.html', () => {
    const path = 'docs/dropdown.html';
    const pathPrefix = path.replace(/\//g, '_');

    cy.visit(path);

    {
        const buttonSelector = '#simple-example .aui-dropdown2-trigger[aria-controls=example-dropdown]';
        cy.get(buttonSelector).click();

        // FIXME 1 of 2: I couldn't trigger the hover highlight behavior on menu item - screenshot does not include that
        const mouseoverSelector = '#example-dropdown .aui-dropdown2-radio[aria-checked="true"]';
        cy.get(mouseoverSelector).trigger('mouseover');

        const screenshotSelector = '#simple-example';
        cy.get(screenshotSelector).matchImageSnapshot(`${pathPrefix}_${screenshotSelector}`);
    }

    {
        const buttonSelector = '#submenu-example .aui-dropdown2-trigger[aria-controls=has-submenu]';
        cy.get(buttonSelector).click();

        // FIXME 2 of 2: Figure out why this one triggers hover highlight in cypress
        const mouseoverSelector = '#has-submenu .aui-dropdown2-sub-trigger';
        cy.get(mouseoverSelector).first().trigger('mouseover');

        const screenshotSelector = '#submenu-example';
        cy.get(screenshotSelector).matchImageSnapshot(`${pathPrefix}_${screenshotSelector}`);
    }
});

it('docs/flag.html', () => {
    const path = 'docs/flag.html';
    const pathPrefix = path.replace(/\//g, '_');

    const screenshotSelector = '#aui-flag-container';

    cy.visit(path);
    cy.viewport(1000, 1000);

    cy.get('#next-flag-show-button').click().click().click().click().click();
    cy.get(screenshotSelector).matchImageSnapshot(`${pathPrefix}_${screenshotSelector}`);
});
