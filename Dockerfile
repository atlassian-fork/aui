FROM atlassianlabs/docker-node-jdk-chrome-firefox:2019-08-26

WORKDIR /usr/src/app

ONBUILD COPY .npmrc /usr/src/app/.npmrc
ONBUILD COPY package.json /usr/src/app/package.json
ONBUILD COPY yarn.lock /usr/src/app/yarn.lock
ONBUILD RUN yarn install --frozen-lockfile --ignore-scripts
ONBUILD COPY . /usr/src/app
ONBUILD RUN yarn prepublishOnly

EXPOSE 7000
EXPOSE 8000
