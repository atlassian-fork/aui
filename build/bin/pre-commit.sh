#!/bin/sh
echo "Running pre-commit hook:"
lint() {
    echo "Gathering files..."
    diff=$(git diff --cached --name-only --diff-filter=ACM | grep -E '(.js)$')
    if [ -z "$diff" ]; then
        echo "...no JS changes found. Done."
        exit 0
    fi
    echo "...linting"
    files="$(echo $diff | tr ' ' ',')"
    yarn pre-commit-lint -s -- --files="$files"
    exit $?
}
lint
