#!/usr/bin/env bash
#set -x
#set -e

TAG="do-not-use"

self=${0##*/}
usage=" USAGE: ${self} [--dry] [-t <tag>] [--npmauthtoken <auth_token>] <dir> <version>
  <dir>     : relative path to location where the code is checked out
  <version> : version of the release

  Builds and deploys AUI version

  Options:
    --dry <dry_run>             : {Boolean} if <dry_run> set to true - logs actions but does not perform them.
    -t, --tag <tag>             : Tag used in NPM registry. Defaults to '${TAG}'
    --npmauthtoken <auth_token> : Updates npm registry _authToken to given value
  "

#--[ Process Arguments ]----
while [ "${1:0:1}" == "-" ]; do
  case ${1:1} in
    t|-tag)
      TAG="${2}"
      shift
    ;;
    -npmauthtoken)
      npm_auth_token="${2}"
      shift
    ;;
    -dry)
      dry_run="${2}"
      shift # past argument
    ;;
    h|-help)
      echo "$usage"
      exit 0
    ;;
    *)    # unknown option
      echo "Unknown argument: -${1}"
      echo "$usage"
      exit 0
    ;;
  esac
  shift # past argument or value
done

if [ -n "$dry_run" ]; then
  if [ "$dry_run" != "true" ]; then
    unset dry_run
  else
    echo "DRY run mode enabled."
  fi
fi

if [ ${#@} -lt 2 ]; then echo "$usage"; exit 1; fi

CHECKOUT_DIR="${1}"
version="${2}"

# Set and check the working directory
cd "$CHECKOUT_DIR"
echo "$ cd $(pwd)"

CHECKOUT_DIR=$(pwd)
auidir="${CHECKOUT_DIR}"

##
# Prep the release space
#
if [ ! -d "${auidir}/.tmp" ]; then
    mkdir "${auidir}/.tmp"
fi

if [ -n "${npm_auth_token}" ]; then
    echo "Set NPM autentication"
    npm set //registry.npmjs.org/:_authToken="${npm_auth_token}"

# todo: ping npmjs to ensure can publish there
# todo: ping npm-private to ensure can publish there
# todo: ping docs microsite to ensure can publish there
fi

cd "${auidir}/packages/core"
echo "$ cd $(pwd)"

##
# Build and deploy the Node package
# Generation of files is thanks to the prepublishOnly step in package.json
#
echo "Build and deploy the Node package"
echo "$ pwd $(pwd)"

if [ -z "${dry_run}" ]; then
    npm publish --@atlassian:registry=https://registry.npmjs.org --tag "${TAG}"
    npm publish --@atlassian:registry=https://packages.atlassian.com/api/npm/atlassian-npm/ --tag "${TAG}"
else
    printf "%s\n"  "pwd: $(pwd)"
    npm publish --@atlassian:registry=https://registry.npmjs.org --dry-run --tag "${TAG}"
    npm publish --@atlassian:registry=https://packages.atlassian.com/api/npm/atlassian-npm/ --dry-run --tag "${TAG}"
fi

##
# Build and deploy the flat distribution
# NOTE: this also gets built as a prepublishOnly step of the core package, therefore we
#       lean on the side-effects of releasing to npm to avoid building this twice.
#       Only during the dry-run we manually build the dist as npm publish was never actually executed
echo "Build and deploy the flat distribution"
echo "$ pwd $(pwd)"

distzip="${auidir}/.tmp/aui-flat-pack-${version}.zip"

echo "$ zip -r ${distzip} ./dist"
zip -r "${distzip}" ./dist

if [ -z "${dry_run}" ]; then
    NODE_ENV=production \
    mvn deploy:deploy-file --batch-mode -DskipTests \
                           -DgroupId=com.atlassian.aui \
                           -DartifactId=aui-flat-pack \
                           -Dversion="${version}" \
                           -Dpackaging=zip \
                           -Dfile="${distzip}" \
                           -DrepositoryId=maven-atlassian-com \
                           -Durl=https://packages.atlassian.com/maven/public
else
    printf "%s\n"  "pwd: $(pwd)"
    printf "%s\n"  "NODE_ENV=production"
    printf "%s\n" "mvn deploy:deploy-file --batch-mode -DskipTests
           -DgroupId=com.atlassian.aui
           -DartifactId=aui-flat-pack
           -Dversion=${version}
           -Dpackaging=zip
           -Dfile=${distzip}
           -DrepositoryId=maven-atlassian-com
           -Durl=https://packages.atlassian.com/maven/public"
fi

cd "${auidir}/p2"
echo "$ cd $(pwd)"

##
# Build and deploy the P2 plugin
#
echo "Build and deploy the P2 plugin"
echo "$ pwd $(pwd)"

if [ -z "${dry_run}" ]; then
    NODE_ENV=production \
    mvn clean deploy --batch-mode -DskipTests
else
    printf "%s\n"  "pwd: $(pwd)"
    printf "%s\n"  "NODE_ENV=production \\" "mvn clean package --batch-mode -DskipTests"
    NODE_ENV=production \
    mvn clean package --batch-mode -DskipTests
fi
