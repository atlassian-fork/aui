const webpack = require('webpack');
const webpackConfig = require('./docs.webpack.config');

module.exports = (opts) => function buildFrontend(done) {
    const finalConfig = Object.assign(webpackConfig, opts);
    webpack(finalConfig, function (err, stats) {
        let info = stats.toJson();
        if (err || stats.hasErrors()) {
            let errors = [].concat(err).concat(info.errors);
            console.error('webpack compilation produced errors', errors);
            done(errors);
        } else {
            // if (stats.hasWarnings()) {
            //     console.warn('webpack compilation produced warnings', info.warnings);
            // }
            done();
        }
    });
};
