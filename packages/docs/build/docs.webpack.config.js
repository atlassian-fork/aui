const path = require('path');
const merge = require('webpack-merge');

const { librarySkeleton, libraryExternals } = require('@atlassian/aui-webpack-config/webpack.skeleton');
const docsSrc = path.resolve(__dirname, '../entry/aui-docs.js');

module.exports = merge([
    librarySkeleton,

    {
        entry: {
            'index': docsSrc,
        },

        externals: [libraryExternals.jqueryExternal],

        output: {
            path: path.resolve('dist')
        },

        optimization: {
            splitChunks: {
                minSize: Infinity,
                chunks: 'all',
                maxAsyncRequests: Infinity,
                maxInitialRequests: Infinity,
                name: true,
            }
        },
    }
]);
