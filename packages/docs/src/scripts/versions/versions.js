// should be preloaded; see main-layout.hbs
const versionPromise = window.jQuery.ajax('/aui/versions.json');

function getVersions() {
    return versionPromise.then(function(result) {
        return result;
    });
}

function buildLatestStableVersionsHtml(versions) {
    return versions.map((version, i) => {
        return `
            <aui-item-link href="/aui/${version}">
                ${version}
                ${i === 0 ? '<span class="aui-lozenge aui-lozenge-subtle aui-lozenge-success">Latest</span>' : ''}
            </aui-item-link>`.trim();
    }).join('');
}

window.document.addEventListener('DOMContentLoaded', () => {
    const dropdownContainer = document.getElementById('versions-dropdown');
    const latestVersionEls = Array.from(document.querySelectorAll('.latest-version'));

    if (dropdownContainer) {
        getVersions()
            .then(versionData => buildLatestStableVersionsHtml(versionData.latest))
            .then(html => {
                let section = document.getElementById('stable-versions-section');
                if (!section) {
                    section = document.createElement('aui-section');
                    section.setAttribute('label','Stable versions');
                    section.id = 'stable-versions-section';
                    dropdownContainer.querySelector('aui-section').insertAdjacentElement('beforebegin', section);
                }
                // a silly thing; we can't directly insert HTML in to these elements because of their light DOM.
                section.querySelector('[role]').innerHTML = html;
            })
    }

    if (latestVersionEls.length) {
        getVersions()
            .then(versionData => versionData.latest[0])
            .then(version => {
                latestVersionEls.forEach(element => element.innerHTML = version)
            })
    }
});
