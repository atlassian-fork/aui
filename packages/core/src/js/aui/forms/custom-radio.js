import skate from 'skatejs';
import jQuery from 'jquery';

/**
 * Allows us to add a new DOM element for rendering ADG styled radio glyphs,
 * so we can get our desired aesthetic without having to rely on a specific markup pattern.
 */
skate('radio', {
    type: skate.type.CLASSNAME,
    attached: function(el) {
        jQuery('input[type=radio]', el).each(function(i, radio) {
            jQuery('<span class="aui-form-glyph"></span>').insertAfter(radio);
        });
    },
    detached: function(el) {
        jQuery('.aui-form-glyph', el).remove();
    }
});
