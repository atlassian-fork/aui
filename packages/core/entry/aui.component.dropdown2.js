import './internal/iconfont'; // todo: should only import what's needed from iconography
import './aui.component.layer';
import './aui.component.trigger';
import '@atlassian/aui/src/less/dropdown2.less';
export * from '@atlassian/aui/src/js/aui/dropdown2';
