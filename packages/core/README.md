# AUI - Atlassian UI

A set of components for building user interfaces in Atlassian products and services.

## Documentation

Thorough documentation is available at [the AUI website](https://aui.atlassian.com/).

* [Component documentation](https://aui.atlassian.com/latest/)
* [Changelog](https://bitbucket.org/atlassian/aui/src/master/CHANGELOG.md?at=master)

## Compatibility

Caveats
-------

- Chrome latest stable
- Firefox latest stable
- Safari latest stable (on OS X only)
- IE 11+

## How do you get it?

Consuming the AUI library is supported through a few methods:

### Install as a Node package

AUI is released to [npmjs.com](https://www.npmjs.com/package/@atlassian/aui).
Install it through your favourite package manager:

`npm install @atlassian/aui`
`yarn add @atlassian/aui`

In the Node package, you will find:

  * `dist/` contains pre-compiled javascript and css. This is the simplest way to use AUI.

  * `src/` contains the raw JavaScript and LESS sources. It's unlikely you'll require these directly.

### Install as an Atlassian plugin

AUI can be used as [an Atlassian P2 plugin](https://developer.atlassian.com/server/framework/atlassian-sdk/plugin-framework/).
This plugin requires Spring Scanner 2 to be available in the runtime it is installed in.

All Atlassian Server products come with AUI pre-installed, so you don't need to do much to re-use it in your plugin.

Each AUI component has a `web-resource` key you can include it by. Consult each component's documentation
on [the AUI website](https://aui.atlassian.com/latest/) for the key.

### Download the distribution

AUI distributions are released as a zip file called the
[aui-flat-pack](https://packages.atlassian.com/maven-public/com/atlassian/aui/aui-flat-pack/), hosted
through Atlassian's Maven nexus.
Note that this is equivalent to the `dist/` folder available in the Node package.

### Consume through a CDN

Use of AUI is not officially supported through a Content Delivery Network (CDN).
However, because AUI is published to npmjs.com, the AUI distributions are also published through
some public CDN services such as:

* [cdnjs.com](https://cdnjs.com/libraries/aui)
* [unpkg.com](https://unpkg.com/@atlassian/aui@latest/)


## Raising issues

Raise bugs or feature requests in the [AUI project](https://ecosystem.atlassian.net/browse/AUI).

## License

AUI is an Atlassian Developer Asset and is released under the [Atlassian Developer Terms license](https://developer.atlassian.com/platform/marketplace/atlassian-developer-terms/).
Some assets are released under the [Atlassian Design Guidelines (ADG) license](https://atlassian.design/server/license/).
See the [licenses directory](https://bitbucket.org/atlassian/aui/src/master/licenses/) for information about AUI, ADG, and included libraries.
