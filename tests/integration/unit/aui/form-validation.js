import $ from '@atlassian/aui/src/js/aui/jquery';
import { findAllTooltips } from '@atlassian/aui/src/js/aui/tooltip';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import validator from '@atlassian/aui/src/js/aui/form-validation';
import {
    afterMutations,
    dispatch,
} from '../../helpers/all';

describe('aui/form-validation', function () {
    var clock;

    afterEach(function () {
        findAllTooltips().remove();
    });

    function makeValidationInput (attributes, parent, inputHTML) {
        var html = inputHTML ? inputHTML : '<input type="text">';
        var $input = $(html);

        attributes = $.extend({}, attributes, {'data-aui-validation-field': '', 'data-aui-validation-when': 'keyup'});

        $.each(attributes, function (key, value) {
            $input.attr(key, value);
        });

        $(parent).append($input);
        skate.init($input);
        return $input;
    }

    function errorsOnPage() {
        return $('.error').length > 0;
    }

    function firstErrorOnPage () {
        return $('.error li').first().contents()[1].nodeValue;
    }

    function firstDescriptionOnPage () {
        return $('.description').first().text();
    }

    function typeMessage ($input, messageToType) {
        $input[0].focus();
        $input.val(messageToType);
        dispatch('keyup', $input[0]);
    }

    function fieldIsValid ($input) {
        return $input.attr('data-aui-validation-state') === 'valid';
    }

    function fieldIsInvalid ($input) {
        return $input.attr('data-aui-validation-state') === 'invalid';
    }

    function fieldIsUnvalidated ($input) {
        return $input.attr('data-aui-validation-state') === 'unvalidated';
    }

    function iconsOnPage () {
        return $('[data-aui-notification-error] ~ .error .aui-icon-notification').length + $('[data-aui-notification-info] + .aui-icon-notification').length;
    }

    function errorIconsOnPage () {
        return $('[data-aui-notification-error]').length;
    }

    function successIconsOnPage () {
        return $('[data-aui-notification-success]').length - $('[data-aui-notification-success][data-aui-notification-error]').length;
    }

    function validateInput ($input) {
        validator.validate($input);
        clock.tick(1000); //Notifications appearing is not synchronous
    }

    it('global', function () {
        expect(AJS.formValidation.toString()).to.equal(validator.toString());
    });

    it('AMD module', function (done) {
        amdRequire(['aui/form-validation'], function (amdModule1) {
            expect(amdModule1).to.equal(validator);
            done();
        });
    });

    describe('Form validation Unit Tests', function () {
        beforeEach(function () {
            try {
                //Register validators for general usage
                validator.register(['alwaysinvalidate'], function (field) {
                    field.invalidate('Invalid');
                });

                validator.register(['slowinvalidator'], function (field) {
                    setTimeout(function () {
                        field.invalidate();
                    }, 2000);
                });

            } catch (e) {
                //we expect an error as we try to register this plugin every time setup is called.
                if (e.name !== 'FormValidationPluginError') {
                    throw e;
                }
            }
            clock = sinon.useFakeTimers();
        });

        afterEach(function () {
            clock.restore();
        });

        it('Input fields start with unvalidated class', function () {
            var $input = makeValidationInput({
                'data-aui-validation-minlength': '20'
            }, $('#test-fixture'));

            expect(fieldIsUnvalidated($input)).to.be.true;
        });

        it('Input field becomes validated after typing valid message', function () {
            var $input = makeValidationInput({
                'data-aui-validation-minlength': '20'
            }, $('#test-fixture'));

            typeMessage($input, 'This is a message longer than twenty characters');
            expect(fieldIsValid($input)).to.be.true;
        });

        it('Input field becomes invalid after typing invalid message', function () {
            var $input = makeValidationInput({
                'data-aui-validation-minlength': '100'
            }, $('#test-fixture'));

            typeMessage($input, 'This is a message shorter than one hundred characters');
            expect(fieldIsInvalid($input)).to.be.true;
        });

        it('Input field becomes invalid if one or two validation functions return invalid', function () {
            var $input = makeValidationInput({
                'data-aui-validation-minlength': '5',
                'data-aui-validation-dateformat': 'Y-m-d'
            }, $('#test-fixture'));

            typeMessage($input, 'A');
            expect(fieldIsInvalid($input)).to.be.true;
            typeMessage($input, 'AAAAAAAAAA');
            expect(fieldIsInvalid($input)).to.be.true;
        });

        describe('Date validation', function() {

            const goodDates = [
                '2016-01-01',
                '2016-10-09',
                '2016-07-29',
                '2016-01-30',
                '2016-01-3',
                '2016-01-31',
                '2016-01-20',
                '2016-1-3',
                '2016-1-01',
                '2016-01-10',
                '9999-1-1',
                // this is valid because date validation is regex based.
                // we may introduce full date parsing in the future:
                '2015-02-31',
            ];

            const badDates = [
                '2016-0-20',
                '2016-1-00',
                '2016-1-0',
                '2016-1-99',
                '2016-13-01',
                'singularity',
                '123456',
            ];

            const validateDateWithExpectedResult = function(date, shouldBeValid) {

                it(`${date} ${shouldBeValid ? 'should' : 'should not'} be valid`, function(done) {
                    const $input = makeValidationInput({
                        'data-aui-validation-minlength': '5',
                        'data-aui-validation-dateformat': 'Y-m-d'
                    }, $('#test-fixture'));

                    typeMessage($input, date);
                    expect(fieldIsValid($input)).to.equal(shouldBeValid);
                    done();
                });

            }

            goodDates.forEach(date => validateDateWithExpectedResult(date, true));
            badDates.forEach(date => validateDateWithExpectedResult(date, false));

        });

        it('Tooltip is shown when field is invalidated', function (done) {
            var $input = makeValidationInput({
                'data-aui-validation-maxlength': '20'
            }, $('#test-fixture'));

            typeMessage($input, 'This is a message longer than twenty characters');
            afterMutations(function () {
                expect(errorsOnPage()).to.be.true;
                done();
            });
        });

        it('Icon is shown when icons needing fields are invalidated', function () {
            var $textInput = makeValidationInput({
                'data-aui-validation-minlength': '20'
            }, $('#test-fixture'), '<input type="text">');
            typeMessage($textInput, 'Too short');
            expect(iconsOnPage()).to.equal(1);

            typeMessage($textInput, 'This is a message longer than twenty characters');
            expect(iconsOnPage()).to.equal(0);


            var $passwordInput = makeValidationInput({
                'data-aui-validation-minlength': '20'
            }, $('#test-fixture'), '<input type="password">');
            typeMessage($passwordInput, 'Too short');
            expect(iconsOnPage()).to.equal(1);

            typeMessage($passwordInput, 'This is a message longer than twenty characters');
            expect(iconsOnPage()).to.equal(0);

            var $textareaInput = makeValidationInput({
                'data-aui-validation-minlength': '20'
            }, $('#test-fixture'), '<textarea>');
            typeMessage($textareaInput, 'Too short');
            expect(iconsOnPage()).to.equal(1);

            typeMessage($textareaInput, 'This is a message longer than twenty characters');
            expect(iconsOnPage()).to.equal(0);
        });

        it('Invalidation is added to fields that are divs', function () {
            var $input = makeValidationInput({
                'data-aui-validation-alwaysinvalidate': '20'
            }, $('#test-fixture'), '<div>');

            validateInput($input);
            expect(fieldIsInvalid($input)).to.be.true;

        });

        it('Plugged in validators can be used by fields', function () {
            validator.register(['testvalidator'], function (field) {
                if (field.el.value.length !== 3) {
                    field.invalidate('not length three');
                } else {
                    field.validate();
                }
            });

            var $input = makeValidationInput({
                'data-aui-validation-testvalidator': ''
            }, $('#test-fixture'));

            typeMessage($input, '1234');
            expect(fieldIsInvalid($input)).to.be.true;
            typeMessage($input, '123');
            expect(fieldIsValid($input)).to.be.true;
        });

        it('Manual revalidation works', function () {
            var $input = makeValidationInput({
                'data-aui-validation-minlength': '20'
            }, $('#test-fixture'));
            $input.val('too short');
            validateInput($input);
            expect(fieldIsInvalid($input)).to.be.true;
        });

        it('Whole form validation triggers validsubmit', function () {
            expect(1);

            var $form = $('<form></form>');
            $('#test-fixture').append($form);

            var $input = makeValidationInput({
                'data-aui-validation-minlength': '1'
            }, $form);

            typeMessage($input, 'this message is long enough');

            $form.one('aui-valid-submit', function (e) {
                e.preventDefault();
                expect(true).to.be.true;
            });

            $form.trigger('submit');
        });

        it('A field with the "watchfield" argument should have validation triggered when the watched field would trigger validation', function () {
            var $input1 = $('<input id="input1"/>').appendTo('#test-fixture');
            var $input2 = makeValidationInput({
                'data-aui-validation-matchingfield': 'input1',
                'data-aui-validation-watchfield': 'input1'
            }, $('#test-fixture'));

            typeMessage($input1, 'mismatched message');
            typeMessage($input2, 'matching message');

            expect(fieldIsInvalid($input2)).to.be.true;
            typeMessage($input1, 'matching message');
            expect(fieldIsValid($input2)).to.be.true;
        });

        it('Cannot register plugins with reserved arguments', function () {
            var registeredValidator = validator.register(['watchfield'], function () {});
            expect(registeredValidator).to.be.false;
        });

        it('Custom error messages display correctly', function (done) {
            var maxValue = 20;
            var customMessage = 'Custom message, needs to be less than ';
            var customMessageUnformatted = `${customMessage}{0}`;
            var customMessageFormatted = customMessage + maxValue;

            var $input = makeValidationInput({
                'data-aui-validation-max': maxValue,
                'data-aui-validation-max-msg': customMessageUnformatted
            }, $('#test-fixture'));

            typeMessage($input, '21');

            afterMutations(function () {
                expect(fieldIsInvalid($input)).to.be.true;
                expect(firstErrorOnPage()).to.equal(customMessageFormatted);
                done();
            });
        });

        it('Custom error messages can be changed after declaration', function (done) {
            var firstMessage = 'First custom message';
            var secondMessage = 'Second custom message';

            var $input = makeValidationInput({
                'data-aui-validation-max': '20',
                'data-aui-validation-max-msg': firstMessage
            }, $('#test-fixture'));

            $input[0].focus();
            typeMessage($input, '21');
            afterMutations(function () {
                expect(fieldIsInvalid($input)).to.be.true;
                expect(firstErrorOnPage()).to.equal(firstMessage);

                $input.attr('data-aui-validation-max-msg', secondMessage);
                $input[0].focus();
                typeMessage($input, '22');
                afterMutations(function () {
                    expect(fieldIsInvalid($input)).to.be.true;
                    expect(firstErrorOnPage()).to.equal(secondMessage);

                    done();
                });
            });
        });

        it('Info notifications should be rendered with correct message', function () {
            var infoMessage = 'Enter a number lower than 20';

            var $input = makeValidationInput({
                'data-aui-validation-max': 20,
                'data-aui-notification-info': infoMessage
            }, $('#test-fixture'));
            skate.init($input[0]);

            expect(firstDescriptionOnPage()).to.equal(infoMessage);
        });

        it('Errors and info notifications stack correctly', function (done) {

            var infoMessage = 'Enter a number lower than 20';
            var errorMessage = 'Must be a number lower than 20';

            var $input = makeValidationInput({
                'data-aui-validation-max': 20,
                'data-aui-validation-max-msg': errorMessage,
                'data-aui-notification-info': infoMessage
            }, $('#test-fixture'));

            $input[0].focus();
            typeMessage($input, '21');
            afterMutations(function () {
                expect(firstErrorOnPage()).to.equal(errorMessage);
                done();
            });
        });

        it('Fields that validate slowly show success indicators when they are validated', function () {
            validator.register(['slowvalidator'], function (field) {
                setTimeout(function () {
                    field.validate();
                }, 2000);
            });


            var $input = makeValidationInput({
                'data-aui-validation-slowvalidator': ''
            }, $('#test-fixture'));
            typeMessage($input, '1234');
            clock.tick(1000);
            expect(successIconsOnPage()).to.equal(0);
            clock.tick(3000);
            expect(successIconsOnPage()).to.equal(1);
        });

        it('Fields that validate slowly show spinners when validating', function () {
            validator.register(['slowvalidator'], function (field) {
                setTimeout(function () {
                    field.validate();
                }, 2000);
            });

            const $input = makeValidationInput({
                'data-aui-validation-slowvalidator': ''
            }, $('#test-fixture'));
            expect($input.next().length, 'there should be no spinnner appended after input').to.equal(0);

            typeMessage($input, '1234');
            clock.tick(1000);
            expect($input.next().get(0).tagName, 'there should be a spinnner appended after input').to.equal('AUI-SPINNER');
            clock.tick(3000);
            expect($input.next().length, 'there should be no spinnner appended after input').to.equal(0);
        });

        it('Fields that validate slowly show errors when they are invalidated', function () {
            var $input = makeValidationInput({
                'data-aui-validation-slowinvalidator': ''
            }, $('#test-fixture'));

            typeMessage($input, '1234');
            clock.tick(1000);
            expect(errorIconsOnPage()).to.equal(0);
            clock.tick(3000);
            expect(errorIconsOnPage()).to.equal(1);
            expect(successIconsOnPage()).to.equal(0);
        });

        it('Fields can have validation parameters changed after delcaration', function () {
            var $input = makeValidationInput({
                'data-aui-validation-pattern': 'Right message'
            }, $('#test-fixture'));

            typeMessage($input, 'Wrong message');
            expect(fieldIsValid($input)).to.be.false;

            typeMessage($input, 'Right message');
            expect(fieldIsValid($input)).to.be.true;

            $input[0].setAttribute('data-aui-validation-pattern', 'New right message');
            typeMessage($input, 'Wrong message');
            clock.tick(100);
            expect(fieldIsValid($input)).to.be.false;

            typeMessage($input, 'Right message');
            clock.tick(100);
            expect(fieldIsValid($input)).to.be.false;

            typeMessage($input, 'New right message');
            clock.tick(100);
            expect(fieldIsValid($input)).to.be.true;
        });

        it('should be possible to set min/max to the same value', function () {
            var exactLength = 20;
            var $input = makeValidationInput({
                minlength: exactLength,
                maxlength: exactLength
            }, $('#test-fixture'));

            typeMessage($input, 'Short message');
            expect(fieldIsInvalid($input)).to.be.true;
            expect($input.data('aui-notification-error')[0]).to.contain(`exactly ${exactLength}`);

            typeMessage($input, 'This is a message longer than twenty characters');
            expect(fieldIsInvalid($input)).to.be.true;

            typeMessage($input, 'Exactly how many characters are needed'.substr(0, exactLength));
            expect(fieldIsValid($input)).to.be.true;
        });

    });

    describe('Form validation basic validators', function () {
        function setupPasswordFields() {
            var $password1 = $('<input type="password" id="password1">').appendTo($('#test-fixture'));
            var $password2 = makeValidationInput({
                'data-aui-validation-watch': 'password1',
                'data-aui-validation-matchingfield': 'password1'
            }, $('#test-fixture'), '<input type="password">');

            return {
                $password1: $password1,
                $password2: $password2
            };
        }

        function setupPasswordAndTextFields() {
            var $textField = $('<input type="text" id="textField">');
            $('#test-fixture').append($textField);

            var $passwordField = makeValidationInput({
                'data-aui-validation-watch': 'textField',
                'data-aui-validation-matchingfield': 'textField'
            }, $('#test-fixture'), '<input type="password">');

            return {
                $textField: $textField,
                $passwordField: $passwordField
            };
        }

        function setupTextFields() {
            var $textField1 = $('<input type="text" id="textField">');
            $('#test-fixture').append($textField1);

            var $textField2 = makeValidationInput({
                'data-aui-validation-watch': 'textField',
                'data-aui-validation-matchingfield': 'textField'
            }, $('#test-fixture'), '<input type="text">');

            return {
                $textField1: $textField1,
                $textField2: $textField2
            };
        }

        it('Required validator works', function (done) {
            var $input = makeValidationInput({
                'data-aui-validation-required': 'required'
            }, $('#test-fixture'));

            typeMessage($input, 'Something');
            afterMutations(function () {
                expect(fieldIsValid($input)).to.be.true;
                done();
            });

        });

        it('Matching field validator should not leak password values', function (done) {
            var fields = setupPasswordFields();

            typeMessage(fields.$password1, 'password123');
            typeMessage(fields.$password2, 'password456');

            afterMutations(function () {
                expect(errorsOnPage(), 'There is a tipsy on the page after typing mismatched passwords');

                var message = firstErrorOnPage();
                var messageContainsPassword1 = message.indexOf('password123') !== -1;
                var messageContainsPassword2 = message.indexOf('password456') !== -1;

                expect(messageContainsPassword1).to.be.false;
                expect(messageContainsPassword2).to.be.false;
                done();
            });
        });

        it('Matching field validator should not leak password values when one field is a password and the other is not', function (done) {
            var fields = setupPasswordAndTextFields();

            typeMessage(fields.$textField, 'inputA');
            typeMessage(fields.$passwordField, 'inputB');


            afterMutations(function () {
                expect(errorsOnPage(), 'There is a tipsy on the page after typing mismatched inputs');

                var message = firstErrorOnPage();
                var messageContainsPassword1 = message.indexOf('inputA') !== -1;
                var messageContainsPassword2 = message.indexOf('inputB') !== -1;

                expect(messageContainsPassword1).to.be.false;
                expect(messageContainsPassword2).to.be.false;
                done();
            });
        });

        it('Matching field validator messages should contain the contents of both messages', function (done) {
            var fields = setupTextFields();

            typeMessage(fields.$textField1, 'inputA');
            typeMessage(fields.$textField2, 'inputB');

            afterMutations(function () {
                var message = firstErrorOnPage();
                var messageContainsInput1 = message.indexOf('inputA') !== -1;
                var messageContainsInput2 = message.indexOf('inputB') !== -1;

                expect(messageContainsInput1).to.be.true;
                expect(messageContainsInput2).to.be.true;
                done();
            });
        });

        it('AUI-3496 - If all fields in the form have the "novalidate" attribute submitting causes an infinite loop.', function () {
            var input = document.createElement('input');
            var form = document.createElement('form');
            var submitted = false;

            form.setAttribute('class', 'aui');
            const skateForm = skate.init(form);
            document.body.appendChild(skateForm);

            input.setAttribute('type', 'text');
            input.setAttribute('data-aui-validation-field', '');
            input.setAttribute('data-aui-validation-minlength', '10');
            input.setAttribute('data-aui-validation-novalidate', 'novalidate');
            input.value = 'asdf';
            form.appendChild(skate.init(input));

            $(form).on('submit', function (e) {
                e.preventDefault();
                submitted = true;
            }).trigger('submit');

            expect(submitted).to.equal(true);
            document.body.removeChild(skateForm);
        });
    });
});
