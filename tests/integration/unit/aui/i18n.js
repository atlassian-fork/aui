import { I18n } from '@atlassian/aui/src/js/aui/i18n';

describe('aui/i18n', function () {
    it('globals', function () {
        expect(AJS.I18n.toString()).to.equal(I18n.toString());
    });

    it('return key test', function () {
        expect(I18n.getText('test.key')).to.equal('test.key');
    });

    describe('returns the value for a defined key', function () {
        beforeEach(function () {
            I18n.keys['test.key'] = 'This is a Value';
            I18n.keys['test.formatting.key'] = 'Formatting {0}, and {1}';

        });

        afterEach(function () {
            delete I18n.keys['test.key'];
            delete I18n.keys['test.formatting.key'];
        });

        it('with no formatting', function () {
            expect(I18n.getText('test.key')).to.equal('This is a Value');
        });

        it('with formatting', function () {
            expect(I18n.getText('test.formatting.key', 'hello', 'world')).to.equal('Formatting hello, and world');
        });
    });
});
