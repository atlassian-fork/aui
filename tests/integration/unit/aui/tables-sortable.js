import tablessortable from '@atlassian/aui/src/js/aui/tables-sortable';

describe('aui/tables-sortable', function () {
    it('globals', function () {
        expect(AJS.tablessortable.toString()).to.equal(tablessortable.toString());
    });
});
