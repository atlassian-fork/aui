import * as deprecate from '@atlassian/aui/src/js/aui/internal/deprecation';

describe('aui/internal/deprecation', function () {
    var fakeStack = [];

    fakeStack.push('Error');
    fakeStack.push('    at getDeprecatedLocation (http://localhost:9876/base/src/js/internal/deprecation.js:67:19)');
    fakeStack.push('    at http://localhost:9876/base/src/js/internal/deprecation.js:40:38');
    fakeStack.push('    at Object.<anonymous> (http://localhost:9876/base/src/js/internal/deprecation.js:91:13)');
    fakeStack.push('    at assertFunctionDeprecated (http://localhost:9876/base/tests/unit/deprecation-test.js:37:22)');
    fakeStack.push('    at Object.<anonymous> (http://localhost:9876/base/tests/unit/deprecation-test.js:70:9)');
    fakeStack.push('    at Object.Test.run (http://localhost:9876/base/node_modules/qunitjs/qunit/qunit.js:203:18)');
    fakeStack.push('    at http://localhost:9876/base/node_modules/qunitjs/qunit/qunit.js:361:10');
    fakeStack.push('    at process (http://localhost:9876/base/node_modules/qunitjs/qunit/qunit.js:1453:24)');
    fakeStack.push('    at http://localhost:9876/base/node_modules/qunitjs/qunit/qunit.js:479:5');

    function getFakeStackMessage () {
        return fakeStack[4];
    }

    var logger;
    var origError = Error;

    beforeEach(function () {
        logger = sinon.stub(console, 'warn');

        window.Error = function () {
            this.stack = getFakeStackMessage();
        };

    });

    afterEach(function () {
        console.warn.restore();
        window.Error = origError;
    });

    function assertDeprecationMessageContainsInputs (message, opts = {}) {
        if (!message) {
            throw new Error('No message was logged.');
        }

        message = message.split('\n')[0];
        expect(message).to.contain(opts.display || 'DISPLAY');
        expect(message).to.contain(opts.alternate || 'ALTERNATE');
        expect(message).to.contain(opts.since || 'SINCE');
        expect(message).to.contain(opts.remove || 'REMOVE');
        expect(message).to.contain(opts.extraInfo || 'EXTRA');
    }

    /**
     * @param {function} checkFnCallback - a callback that should itself invoke the deprecated function.
     *   calling the deprecated function directly would affect the context upon which it is called,
     *   which wouldn't really help test that the deprecation utility was doing its job...
     * @param opts
     */
    function assertFunctionDeprecated (checkFnCallback, opts) {
        logger.reset();
        checkFnCallback();
        expect(logger.callCount).to.equal(1, 'call 1');
        assertDeprecationMessageContainsInputs(logger.args[0] && logger.args[0][0], opts);
        checkFnCallback();
        expect(logger.callCount).to.equal(1, 'call 2');
    }

    function assertNoncallablePropertyDeprecated (obj, deprecatedProp, initialVal, opts) {
        logger.reset();

        var actualValue = obj[deprecatedProp];

        expect(actualValue).to.equal(initialVal, 'non 1');
        expect(logger.callCount).to.equal(1, 'non 2');
        assertDeprecationMessageContainsInputs(logger.args[0] && logger.args[0][0], opts);
        expect(obj[deprecatedProp] = 'a').to.equal('a', 'non 3');
        expect(logger.callCount).to.equal(1, 'non 4');
        expect(obj[deprecatedProp]).to.equal('a', 'non 5');
        obj[deprecatedProp] = actualValue;
    }

    describe('deprecate.fn()', function () {
        var callContext;
        var arg = {};

        function fn(param) {
            callContext(this, param);
        }

        beforeEach(function() {
            callContext = sinon.spy();
        });

        it('can create a deprecated function wrapper', function() {
            var deprecatedFn = deprecate.fn(fn, 'DISPLAY', {
                alternativeName: 'ALTERNATE',
                removeInVersion: 'REMOVE',
                sinceVersion: 'SINCE',
                extraInfo: 'EXTRA',
            });

            expect(deprecatedFn).to.be.a('function');

            assertFunctionDeprecated(() => deprecatedFn(arg));
            expect(callContext.callCount).to.equal(2);
            expect(callContext.args[0][0]).to.equal(undefined, 'the context should not be defined when called bare, because mocha');
            expect(callContext.args[0][1]).to.equal(arg);
        });

        it('can create a deprecated function wrapper that respects context', function() {
            var obj = {};
            var deprecatedFn = deprecate.fn(fn, 'DISPLAY', {
                alternativeName: 'ALTERNATE',
                removeInVersion: 'REMOVE',
                sinceVersion: 'SINCE',
                extraInfo: 'EXTRA',
            });
            obj.deprecatedFnProp = deprecatedFn;

            assertFunctionDeprecated(() => obj.deprecatedFnProp(arg));
            expect(callContext.callCount).to.equal(2);
            expect(callContext.args[0][0]).to.equal(obj, 'the context should be inherited');
            expect(callContext.args[0][1]).to.equal(arg);
        });
    });

    describe('deprecate.prop()', function () {
        var callContext;
        var arg = {};
        var prop = {};
        var obj;

        beforeEach(function() {
            callContext = sinon.spy();
            obj = {
                prop: prop,
                method: function (param) {
                    callContext(this, param);
                }
            };
        });

        it('can deprecate non-callable properties', function () {
            deprecate.prop(obj, 'prop', {displayName: 'DISPLAY', alternativeName: 'ALTERNATE', removeInVersion: 'REMOVE', sinceVersion: 'SINCE', extraInfo: 'EXTRA'});
            assertNoncallablePropertyDeprecated(obj, 'prop', prop);
        });

        it('can deprecate callable properties', function () {
            deprecate.prop(obj, 'method', {
                displayName: 'DISPLAY',
                alternativeName: 'ALTERNATE',
                removeInVersion: 'REMOVE',
                sinceVersion: 'SINCE',
                extraInfo: 'EXTRA',
            });

            assertFunctionDeprecated(() => obj.method(arg));
            expect(callContext.callCount).to.equal(2);
            expect(callContext.args[0][0]).to.equal(obj);
            expect(callContext.args[0][1]).to.equal(arg);
        });
    });

    describe('deprecate.obj()', function () {
        var callContext;
        var arg = {};
        var prop1 = {};
        var prop2 = 'bar';
        var prop3 = Infinity;
        var method = function (param) {
            callContext(this, param);
        };
        var obj;

        beforeEach(function() {
            callContext = sinon.spy();
            obj = {
                prop1,
                prop2,
                prop3,
                method
            };
            deprecate.obj(obj, 'OBJ:', {
                alternativeNamePrefix: 'ALTERNATIVE:',
                removeInVersion: 'REMOVE',
                sinceVersion: 'SINCE',
                extraInfo: 'EXTRA INFO',
            });
        });

        it('can deprecate multiple properties at once', function () {
            assertNoncallablePropertyDeprecated(obj, 'prop1', prop1, {
                display: 'OBJ:prop1',
                alternate: 'ALTERNATIVE:prop1',
                since: 'SINCE',
                remove: 'REMOVE',
                extra: 'EXTRA INFO'
            });
            assertNoncallablePropertyDeprecated(obj, 'prop2', prop2, {
                display: 'OBJ:prop2',
                alternate: 'ALTERNATIVE:prop2',
                since: 'SINCE',
                remove: 'REMOVE',
                extra: 'EXTRA INFO'
            });
            assertNoncallablePropertyDeprecated(obj, 'prop3', prop3, {
                display: 'OBJ:prop3',
                alternate: 'ALTERNATIVE:prop3',
                since: 'SINCE',
                remove: 'REMOVE',
                extra: 'EXTRA INFO'
            });
        });

        it('can deprecate callable properties', function () {
            assertFunctionDeprecated(() => obj.method(arg), {
                display: 'OBJ:method',
                alternate: 'ALTERNATIVE:method',
                since: 'SINCE',
                remove: 'REMOVE',
                extra: 'EXTRA INFO'
            });
            expect(callContext.callCount).to.equal(2);
            expect(callContext.args[0][0]).to.equal(obj);
            expect(callContext.args[0][1]).to.equal(arg);
        });
    });
});

describe('aui/internal/deprecation (defaults)', function () {
    var log;
    var warn;

    beforeEach(function () {
        if (!console) {
            console = {
                log: function () {},
                warn: function () {}
            };
        }
        log = sinon.spy(console, 'log');
        warn = sinon.spy(console, 'warn');
    });

    afterEach(function () {
        console.warn.restore();
        console.log.restore();
    });

    it('default logger is console.warn', function () {
        var deprecatedFn = deprecate.fn(function () {}, 'DISPLAY', {alternativeName: 'ALTERNATE', removeInVersion: 'REMOVE', sinceVersion: 'SINCE', extraInfo: 'EXTRA'});
        deprecatedFn();

        expect(log.callCount).to.equal(0);
        expect(warn.callCount).to.equal(1);
    });
});
