import Label from '@atlassian/aui/src/js/aui/label';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import { INPUT_SUFFIX } from '@atlassian/aui/src/js/aui/internal/constants';
import {
    afterMutations,
    fixtures,
} from '../../helpers/all';

describe('aui/label', function () {
    var label;
    var LABEL_FOR = 'test-for';
    var LABEL_FORM = 'test-form';
    var LABEL_CONTENT = 'test content';

    function createLabel(options) {
        if (!options) {
            options = {}
        }
        var labelFor = options.for ? `for="${options.for}"` : '';
        var labelForm = options.form ? `form="${options.form}"` : '';
        var labelContent = options.content ? options.content : '';
        var dom = fixtures({
            label: `<aui-label ${labelFor} ${labelForm}>${labelContent}</aui-label>`
        });
        skate.init(dom.label);
        return dom.label;
    }

    it('globals', function () {
        expect(AJS.label).to.equal(undefined);
    });

    it('exports', function() {
        expect(Label).to.exist;
        expect(new Label()).to.be.instanceof(HTMLElement);
    });

    describe('Label - construction and initialisation', function () {
        describe('basic initialisation', function () {
            beforeEach(function () {
                label = createLabel({'content': LABEL_CONTENT});
            });

            it('creates a native label child element', function () {
                expect(label._label.tagName).to.equal('LABEL');
            });

            it('moves the content into the child label', function () {
                expect(label._label.textContent).to.equal(LABEL_CONTENT);
            });
        });

        describe('initialising attributes', function () {
            beforeEach(function () {
                label = createLabel({for: LABEL_FOR, form: LABEL_FORM, content: LABEL_CONTENT});
            }) ;

            it('copies the \'for\' attribute to the child label and appends \'input\' to it', function () {
                expect(label._label.getAttribute('for')).to.equal(LABEL_FOR + INPUT_SUFFIX);
            });

            it('copies the \'form\' attribute to the child label', function () {
                expect(label._label.getAttribute('form')).to.equal(LABEL_FORM);
            });
        });
    });

    describe('Label - behaviour', function () {
        it('handles attributes being added to the label', function (done) {
            label = createLabel();
            expect(label._label.hasAttribute('for')).to.be.false;
            expect(label._label.hasAttribute('form')).to.be.false;

            label.setAttribute('for', LABEL_FOR);
            label.setAttribute('form', LABEL_FORM);

            // TODO Remove afterMutations when we upgrade to skatejs 0.14.x/1.0.x since it has
            //      synchronous setAttribute handling.
            afterMutations(function () {
                expect(label._label.getAttribute('for')).to.equal(LABEL_FOR + INPUT_SUFFIX);
                expect(label._label.getAttribute('form')).to.equal(LABEL_FORM);
                done();
            });
        });

        it('handles attributes being modified', function (done) {
            label = createLabel({for: LABEL_FOR, form: LABEL_FORM});
            expect(label._label.getAttribute('for')).to.equal(LABEL_FOR + INPUT_SUFFIX);
            expect(label._label.getAttribute('form')).to.equal(LABEL_FORM);

            var LABEL_FOR_NEW = 'test-for-new';
            label.setAttribute('for', LABEL_FOR_NEW);

            // TODO Remove afterMutations when we upgrade to skatejs 0.14.x/1.0.x since it has
            //      synchronous setAttribute handling.
            afterMutations(function () {
                expect(label._label.getAttribute('for')).to.equal(LABEL_FOR_NEW + INPUT_SUFFIX);

                var LABEL_FORM_NEW = 'test-form-new';
                label.setAttribute('form', LABEL_FORM_NEW);
                afterMutations(function () {
                    expect(label._label.getAttribute('form')).to.equal(LABEL_FORM_NEW);
                    done();
                });
            });
        });

        it('handles attributes being removed from the label', function (done) {
            label = createLabel({for: LABEL_FOR, form: LABEL_FORM});
            expect(label._label.hasAttribute('for')).to.be.true;
            expect(label._label.hasAttribute('form')).to.be.true;

            label.removeAttribute('for');

            // TODO Remove afterMutations when we upgrade to skatejs 0.14.x/1.0.x since it has
            //      synchronous removeAttribute handling.
            afterMutations(function () {
                expect(label._label.hasAttribute('for')).to.be.false;
                label.removeAttribute('form');
                afterMutations(function () {
                    expect(label._label.hasAttribute('form')).to.be.false;
                    done();
                });
            });
        });
    });

});
