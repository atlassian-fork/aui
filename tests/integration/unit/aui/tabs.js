import $ from '@atlassian/aui/src/js/aui/jquery';
import aui from '@atlassian/aui-soy/entry/aui-soy';
import { afterMutations } from '../../helpers/all';
import skate from '@atlassian/aui/src/js/aui/internal/skate';
import skateTemplateHtml from 'skatejs-template-html';
import tabs from '@atlassian/aui/src/js/aui/tabs';

describe('aui/tabs', function () {
    var fixture;
    var DATA_TABS_PERSIST = 'data-aui-persist';
    var STORAGE_PREFIX = '_internal-aui-tabs-';

    function getAnchor (id) {
        return document.querySelector(`[href$="${id}"]`);
    }

    function getKey (id) {
        return getAnchor(id).getAttribute('href');
    }

    function select (id) {
        $('aui-tabs').each(function () {
            this.select(document.getElementById(id));
        });
    }

    function persistAttribute (c) {
        return c.persist || c.persist === '' ?
            ` ${DATA_TABS_PERSIST}=${c.persist ? c.persist : '""'}` :
            '';
    }

    function renderTemplate (c, init) {
        c = $.extend({
            id: 'aui-tabs',
            persist: undefined,
            tabPostfix: ''
        }, c);

        fixture.innerHTML += '' +
            '<aui-tabs id="' + c.id + '" ' + persistAttribute(c) + '>' +
                '<aui-tabs-pane id="tab-lister' + c.tabPostfix + '" title="Lister">+1</aui-tabs-pane>' +
                '<aui-tabs-pane id="tab-rimmer' + c.tabPostfix + '" title="Rimmer">-1</aui-tabs-pane>' +
            '</aui-tabs>';

        // For the tests that call this function more than once
        // consecutively, this will get called the same number of times
        // and causes the tab list items to disappear from the first
        // tab group. It's probably a race condition with `skate.init()`.
        if (init || init === undefined) {
            skate.init(fixture);
        }
    }

    function addTabPane () {
        var pane = document.createElement('aui-tabs-pane');

        pane.id = 'tab-kryten';
        pane.title = 'Kryten';
        pane.textContent = '+∞';

        skateTemplateHtml.wrap(fixture.children[0]).appendChild(pane);
        skate.init(pane);
    }

    function createPersistentKey ($tabGroup) {
        var tabGroupId = $tabGroup.attr('id');
        var value = $tabGroup.attr(DATA_TABS_PERSIST);

        return STORAGE_PREFIX + (tabGroupId ? tabGroupId : '') + (value && value !== 'true' ? '-' + value : '');
    }

    function getPersistent (groupId) {
        // groupId must exist in the dom
        var key = createPersistentKey($('#' + groupId));
        return window.localStorage.getItem(key);
    }

    function storePersistent (groupId, value) {
        // Before custom elements we were using the anchor id, now we're
        // using the list-item id. We use this to get the anchor id.
        value = getKey(value);

        // groupId must exist in the dom
        var key = createPersistentKey($('#' + groupId));
        window.localStorage.setItem(key, value);
    }

    beforeEach(function () {
        fixture = document.getElementById('test-fixture');
    });

    afterEach(function (done) {
        window.localStorage.clear();
        // IE11: Give some time to clean up DOM
        afterMutations(done);
    });

    it('globals', function () {
        expect(AJS.tabs.toString()).to.equal(tabs.toString());
    });

    it('API', function () {
        expect(tabs).to.be.an('object');
        expect(tabs.setup).to.be.a('function');
        expect(tabs.change).to.be.a('function');
    });

    it('hide active tab and shows new tab on click', function () {
        renderTemplate();
        select('tab-rimmer');

        expect($('#tab-lister').is(':visible')).to.be.false;
        expect($('#tab-rimmer').is(':visible')).to.be.true;
    });

    it('setup() will not double-bind event handlers on multiple calls.', function () {
        renderTemplate();
        tabs.setup();
        tabs.setup();

        var clickSpy = sinon.spy();

        $('.tabs-menu a').bind('tabSelect', clickSpy);
        select('tab-rimmer');

        clickSpy.should.have.been.calledOnce;
    });

    it('adding a tab dynamically should work', function () {
        renderTemplate();
        addTabPane();
        select('tab-kryten');

        expect($('#tab-lister').is(':visible')).to.be.false;
        expect($('#tab-rimmer').is(':visible')).to.be.false;
        expect($('#tab-kryten').is(':visible')).to.be.true;
    });

    it('soy template exists', function () {
        expect(aui.tabs).to.be.defined;
    });

    it('soy template basic rendering', function () {
        var html = aui.tabs({
            menuItems: [{
                isActive: true,
                url: '#tab1',
                text: 'Tab 1'
            }, {
                url: '#tab2',
                text: 'Tab 2'
            }],
            paneContent: aui.tabPane({
                isActive: true,
                content: 'Tab 1 Content'
            }) + aui.tabPane({
                content: 'Tab 2 Content'
            })
        });

        var $el = $(html).appendTo($('#test-fixture'));
        expect($el.is('.aui-tabs')).to.be.true;
    });

    it('no id persist=true does not save to local storage', function () {
        renderTemplate({
            persist: 'true'
        });

        select('tab-rimmer');

        expect($('#tab-rimmer').is(':visible')).to.be.true;
        expect(getPersistent('tabgroup1')).to.be.not.defined;
    });

    it('persist with no value saves to local storage', function () {
        renderTemplate({
            id: 'tabgroup1',
            persist: ''
        });

        select('tab-rimmer');

        expect($('#tab-rimmer').is(':visible')).to.be.true;
        expect(getPersistent('tabgroup1')).to.equal(getKey('tab-rimmer'));
    });

    it('persist=true saves to local storage', function () {
        renderTemplate({
            id: 'tabgroup1',
            persist: 'true'
        });

        select('tab-lister');
        select('tab-rimmer');

        expect($('#tab-lister').is(':visible')).to.be.false;
        expect($('#tab-rimmer').is(':visible')).to.be.true;
        expect(getPersistent('tabgroup1')).to.equal(getKey('tab-rimmer'));
    });

    it('persist with unique value saves to local storage', function () {
        renderTemplate({
            id: 'tabgroup1',
            persist: 'user1'
        });

        select('tab-rimmer');

        expect($('#tab-rimmer').is(':visible')).to.be.true;
        expect(getPersistent('tabgroup1')).to.equal(getKey('tab-rimmer'));
    });

    it('persist=false does not save to local storage', function () {
        renderTemplate({
            id: 'tabgroup1',
            persist: 'false'
        });

        select('tab-lister');
        select('tab-rimmer');

        expect($('#tab-lister').is(':visible')).to.be.false;
        expect($('#tab-rimmer').is(':visible')).to.be.true;
        expect(getPersistent('tabgroup1')).to.not.be.defined;
    });

    it('no persist does not save to local storage', function () {
        renderTemplate({
            id: 'tabgroup1'
        });

        select('tab-rimmer');

        expect($('#tab-rimmer').is(':visible')).to.be.true;
        expect(getPersistent('tabgroup1')).to.not.be.defined;
    });

    it('persist=true saves to local storage multiple tab groups', function () {
        renderTemplate({
            id: 'tabgroup1',
            persist: 'true'
        }, false);

        renderTemplate({
            id: 'tabgroup2',
            persist: 'true',
            tabPostfix: '2'
        });

        select('tab-lister');
        select('tab-rimmer');
        select('tab-rimmer2');
        select('tab-lister2');

        expect(getPersistent('tabgroup1')).to.equal(getKey('tab-rimmer'));
        expect(getPersistent('tabgroup2')).to.equal(getKey('tab-lister2'));
    });

    it('persist=false doesnt load from local storage', function () {
        renderTemplate({
            id: 'tabgroup1',
            persist: 'false'
        });

        storePersistent('tabgroup1', 'tab-rimmer');

        expect($('#tab-lister').is(':visible')).to.be.true;
        expect($('#tab-rimmer').is(':visible')).to.be.false;
    });

    it('persist=true loads from local storage', function () {
        renderTemplate({
            id: 'tabgroup1',
            persist: 'true'
        });

        storePersistent('tabgroup1', 'tab-rimmer');
        tabs.setup();

        expect($('#tab-lister').is(':visible')).to.be.false;
        expect($('#tab-rimmer').is(':visible')).to.be.true;
    });

    it('loading from local storage invokes tabSelect handler', function () {
        var tabSelectSpy = sinon.spy();

        renderTemplate({
            id: 'tabgroup1',
            persist: 'true'
        });

        storePersistent('tabgroup1', 'tab-rimmer');
        $('.tabs-menu a').bind('tabSelect', tabSelectSpy);
        tabs.setup();

        tabSelectSpy.should.have.been.calledOnce;
    });

    it('persist=true loads from local storage with multiple tab groups', function () {
        renderTemplate({
            id: 'tabgroup1',
            persist: 'true'
        }, false);

        renderTemplate({
            id: 'tabgroup2',
            persist: 'true',
            tabPostfix: '2'
        });

        storePersistent('tabgroup1', 'tab-rimmer');
        storePersistent('tabgroup2', 'tab-rimmer2');
        tabs.setup();

        expect($('#tabgroup1').find('#tab-lister').is(':visible')).to.be.false;
        expect($('#tabgroup1').find('#tab-rimmer').is(':visible')).to.be.true;
        expect($('#tabgroup2').find('#tab-lister2').is(':visible')).to.be.false;
        expect($('#tabgroup2').find('#tab-rimmer2').is(':visible')).to.be.true;
    });

    describe('responsive', function () {
        var fixture;
        var responsiveTabs;
        var responsiveTabsHtml = '' +
            '<aui-tabs responsive="true">' +
                '<aui-tabs-pane id="horizontal-wrap-first-2" title="Tab - 1 Active">' +
                    '<h2>This is Tab 1</h2>' +
                    '<p>First</p>' +
                '</aui-tabs-pane>' +
                '<aui-tabs-pane id="horizontal-wrap-second-2" title="Tab 2">' +
                    '<h2>This is Tab 2</h2>' +
                    '<p>Second</p>' +
                '</aui-tabs-pane>' +
                '<aui-tabs-pane id="horizontal-wrap-third-2" title="Tab 3">' +
                    '<h2>This is Tab 3</h2>' +
                    '<p>Third</p>' +
                '</aui-tabs-pane>' +
                '<aui-tabs-pane id="horizontal-wrap-fourth-2" title="Tab 4">' +
                    '<h2>This is Tab 4</h2>' +
                    '<p>Fourth</p>' +
                '</aui-tabs-pane>' +
            '</aui-tabs>';

        beforeEach(function () {
            fixture = document.getElementById('test-fixture');
            fixture.innerHTML = responsiveTabsHtml;
            responsiveTabs = fixture.children[0];
            skate.init(fixture);
        });

        it('should use the immediate container width to calculate which tabs to show', function () {
            var tabs = $('.tabs-menu').find('.menu-item');
            var tabsWidth = 0;

            tabs.each(function (i) {
                tabsWidth += $(tabs[i]).outerWidth();
            });

            expect(tabsWidth).to.be.below($(responsiveTabs).width());
        });
    });

    describe('legacy construction', function() {
        let navigateSpy;
        let navigateHandler = e => {
            navigateSpy(e.isDefaultPrevented(), e);
            e.preventDefault();
        };

        beforeEach(function () {
            navigateSpy = sinon.spy();
            $(fixture).on('click', 'a', navigateHandler);
        });

        afterEach(function () {
            $(fixture).off('click', navigateHandler);
        });

        it('binds no special behaviour if the `aui-tabs-disabled` class is added', function () {
            fixture.innerHTML = `<div class="aui-tabs aui-tabs-disabled horizontal">
                <ul class="tabs-menu">
                    <li class="menu-item active-tab">
                        <a id="click-me-for-first" href="#first">Tab 1</a>
                    </li>
                    <li class="menu-item">
                        <a id="click-me-for-second" href="#second">Tab 2</a>
                    </li>
                </ul>
                <div class="tabs-pane active-pane" id="first"></div>
                <div class="tabs-pane" id="second"></div>
            </div>`;
            tabs.setup();

            // nothing should prevent click event defaults here because no behaviour was bound.
            fixture.querySelector('#click-me-for-second').click();
            expect(navigateSpy.callCount).to.equal(1);
            expect(navigateSpy.getCall(0).args[0], 'event default should not be prevented, but was').to.equal(false);

            let activePanel = $(fixture).find('.active-pane').attr('id');
            expect(activePanel).to.not.equal('second');
            expect(activePanel).to.equal('first');
        });

        it('binds no special behaviour if the `aui-tabs-disabled` class is added on a responsive tab', function () {
            fixture.innerHTML = `
            <div style="display: inline-block;width: 100px;">
                <div class="aui-tabs aui-tabs-disabled horizontal-tabs spacetools-nav-secondary" data-aui-responsive>
                    <ul class="tabs-menu">
                        <li class="menu-item active-tab">
                        <a id="click-me-for-first" href="/child1">Tab 1</a>
                        </li>
                        <li class="menu-item">
                        <a id="click-me-for-second" href="/child2">Tab 2</a>
                        </li>
                        <li class="menu-item">
                        <a id="click-me-for-third" href="/child3">Tab 3</a>
                        </li>
                    </ul>
                <div class="tabs-pane active-pane" id="first"></div>
                </div>
            </div> 
                       `;
            tabs.setup();
            // this is a visible tab
            fixture.querySelector('#click-me-for-second').click();
            // this is a dropped down tab
            fixture.querySelector('#click-me-for-second').click();

            expect(navigateSpy.callCount).to.equal(2);

            var isDefaultPreventedOnVisibleTab = navigateSpy.getCall(0).args[0];
            var isDefaultPreventedOnDropdownTab = navigateSpy.getCall(1).args[0];
           
            expect(isDefaultPreventedOnVisibleTab, 'event default should not be prevented, but was').to.equal(false);
            expect(isDefaultPreventedOnDropdownTab, 'event default should not be prevented, but was').to.equal(false);
        });

        // AUI-4920
        it('fails gracefully when a tab link is misconfigured with a hyperlink instead of anchor', function () {
            fixture.innerHTML = `<div class="aui-tabs horizontal">
                <ul class="tabs-menu">
                    <li class="menu-item active-tab">
                        <a id="click-me-for-google" href="https://google.com">Tab 1</a>
                    </li>
                    <li class="menu-item">
                        <a id="click-me-for-bing" href=" http://bing.com">Tab 2</a>
                    </li>
                </ul>
            </div>`;
            expect(tabs.setup).to.not.throw();
        });

        // AUI-4920
        it('does not navigate to a tab hyperlink when it is misconfigured', function () {
            fixture.innerHTML = `<div class="aui-tabs horizontal">
                <ul class="tabs-menu">
                    <li class="menu-item active-tab">
                        <a id="click-me-for-google" href="https://google.com">Tab 1</a>
                    </li>
                    <li class="menu-item">
                        <a id="click-me-for-bing" href=" http://bing.com">Tab 2</a>
                    </li>
                </ul>
            </div>`;
            tabs.setup();

            // the default *should be prevented*, because tabs should only affect the current page,
            // and anything that would cause a navigation event is a misconfiguration.
            fixture.querySelector('#click-me-for-google').click();
            expect(navigateSpy.callCount).to.equal(1);
            expect(navigateSpy.getCall(0).args[0], 'event default should be prevented, but was not').to.equal(true);
        });
    });
});
