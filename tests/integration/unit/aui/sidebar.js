import $ from '@atlassian/aui/src/js/aui/jquery';
import keyCode from '@atlassian/aui/src/js/aui/key-code';
import SidebarFn from '@atlassian/aui/src/js/aui/sidebar';
import sidebarHtml from './sidebar/sidebar-html';
import sidebarHtmlNoSubmenus from './sidebar/sidebar-html-no-submenus';
import CustomEvent from '@atlassian/aui/src/js/aui/polyfills/custom-event';
import {
    afterMutations,
    dispatch,
    focus,
    hover,
    pressKey,
} from '../../helpers/all';

describe('aui/sidebar', function () {
    var sidebar;
    var clock;

    beforeEach(function (done) {
        $('#test-fixture').html(sidebarHtml);

        sidebar = new SidebarFn($('.aui-sidebar'));
        clock = sinon.useFakeTimers();
        // AUI-4322 - force layout to happen now as opposed to on the next animation frame.
        afterMutations(function() {
            done();
        }, 50);
    });

    afterEach(function () {
        clock.restore();
        sidebar._remove();
    });

    it('puts a class on the body', function () {
        expect($(document.body).hasClass('aui-page-sidebar')).to.equal(true);
    });

    it('can be collapsed and uncollapsed via the .collapse() and .expand() methods', function () {
        sidebar.collapse();
        expect(sidebar.isCollapsed()).to.equal(true);
        sidebar.collapse();
        expect(sidebar.isCollapsed()).to.equal(true);
        sidebar.expand();
        expect(sidebar.isCollapsed()).to.equal(false);
    });

    it('should appropriately collapse/expand when calling toggle()', function () {
        var isCollapsed = sidebar.isCollapsed();

        sidebar.toggle();
        expect(sidebar.isCollapsed()).to.equal(isCollapsed = !isCollapsed);
        sidebar.toggle();
        expect(sidebar.isCollapsed()).to.equal(!isCollapsed);
    });

    it('should collapse/expand when the toggle icon is clicked', function () {
        var $toggle = sidebar.$el.find('.aui-sidebar-toggle');
        var isCollapsed = sidebar.isCollapsed();

        $toggle.click();
        expect(sidebar.isCollapsed()).to.equal(isCollapsed = !isCollapsed);
        $toggle.click();
        expect(sidebar.isCollapsed()).to.equal(!isCollapsed);
    });

    it('is toggled when [ is pressed', function () {
        var isCollapsed = sidebar.isCollapsed();

        pressKey('[', {}, document.body);
        expect(sidebar.isCollapsed()).to.equal(isCollapsed = !isCollapsed);
        pressKey('[', {}, document.body);
        expect(sidebar.isCollapsed()).to.equal(!isCollapsed);
    });

    it('is not toggled when [ is pressed with modifiers', function () {
        var isCollapsed = sidebar.isCollapsed();
        var modifiers = {};

        ['control', 'shift', 'meta'].forEach(function (type) {
            modifiers[type] = true;
            pressKey('[', modifiers, document.body);
            expect(sidebar.isCollapsed()).to.equal(isCollapsed, type);
            delete modifiers[type];
        });
    });

    it('is toggled when [ is pressed with alt (german keyboard layout)', function () {
        var isCollapsed = sidebar.isCollapsed();

        pressKey('[', {alt: true}, document.body);
        expect(sidebar.isCollapsed()).to.equal(isCollapsed = !isCollapsed);
        pressKey('[', {alt: true}, document.body);
        expect(sidebar.isCollapsed()).to.equal(!isCollapsed);
    });

    it('is NOT toggled when [ is pressed inside an input', function () {
        ['input', 'select', 'textarea'].forEach(function (type) {
            var isCollapsed = sidebar.isCollapsed();

            var el = document.createElement(type);
            document.body.appendChild(el);
            pressKey('[', {}, el);

            expect(sidebar.isCollapsed()).to.equal(isCollapsed);

            document.body.removeChild(el);
        })
    });

    it('should set aria-expanded when collapsed/expanded', function () {
        sidebar.collapse();
        expect(sidebar.$el.attr('aria-expanded')).to.equal('false');
        sidebar.expand();
        expect(sidebar.$el.attr('aria-expanded')).to.equal('true');
    });

    it('should toggle the aui-sidebar-collapsed class on the <body>', function () {
        sidebar.collapse();
        expect($('body').hasClass('aui-sidebar-collapsed')).to.equal(true);
        sidebar.expand();
        // The sidebar may be in fly-out mode if the browser is too narrow,
        // in which case aui-sidebar-collapsed remains on the <body>.
        expect($('body').hasClass('aui-sidebar-collapsed')).to.equal(sidebar.isViewportNarrow());
    });

    it('should be automatically collapsed/expanded when the browser is resized', function () {
        sidebar.reflow(0, 1000, 2000);
        sidebar.expand();
        sidebar.reflow(0, 1000, 1024);
        expect(sidebar.isCollapsed()).to.equal(true);
        sidebar.reflow(0, 1000, 2000);
        expect(sidebar.isCollapsed()).to.equal(false);
    });

    it('should not be automatically expanded when the browser is resized and event is prevented', function () {
        sidebar.reflow(0, 1000, 2000);
        sidebar.expand();

        sidebar.on('expand-start', function (e) {
            e.preventDefault();
        });

        sidebar.reflow(0, 1000, 1024);
        expect(sidebar.isCollapsed()).to.equal(true);
        sidebar.reflow(0, 1000, 2000);
        expect(sidebar.isCollapsed()).to.equal(true);
    });

    it('should not be automatically collapsed when the browser is resized and event is prevented', function () {
        sidebar.reflow(0, 1000, 2000);
        sidebar.expand();

        sidebar.on('collapse-start', function (e) {
            e.preventDefault();
        });

        sidebar.reflow(0, 1000, 1024);
        expect(sidebar.isCollapsed()).to.equal(false);
        sidebar.reflow(0, 1000, 2000);
        expect(sidebar.isCollapsed()).to.equal(false);
    });

    it('should remain collapsed when collapsed at wide width and browser made wider', function () {
        sidebar.reflow(0, 1000, 2000);
        sidebar.collapse();
        expect(sidebar.isCollapsed()).to.equal(true);
        sidebar.reflow(0, 1000, 3000);
        expect(sidebar.isCollapsed()).to.equal(true);
    });

    it('should expand in fly-out mode when expanded in a narrow browser', function () {
        sidebar.collapse();
        sidebar.reflow(0, 1000, 1024);
        sidebar.expand();
        expect(sidebar.$el.hasClass('aui-sidebar-fly-out')).to.equal(true);

        sidebar.collapse();
        sidebar.reflow(0, 1000, 2000);
        sidebar.expand();
        expect(sidebar.$el.hasClass('aui-sidebar-fly-out')).to.equal(false);
    });

    describe('when scrolling the page horizontally', function () {
        let scrollLeft = 200;

        describe('and the sidebar is docked', function () {
            let scrollTop = 150;

            it('should stay in place on the left of the page', function () {
                sidebar.reflow(scrollTop, 1000, 2000, 1200, scrollLeft);
                expect(sidebar.$wrapper.css('left')).to.equal(`-${scrollLeft}px`);
            });
        });

        describe('and the sidebar is not docked', function () {
            let scrollTop = 0;

            it('should not change the sidebar position', function () {
                // given
                const originalLeft = sidebar.$wrapper.css('left');
                expect(originalLeft).to.be.oneOf(['0px', 'auto']); // different browsers will set different values
                // when
                sidebar.reflow(scrollTop, 1000, 2000, 1200, scrollLeft);
                // then
                expect(sidebar.$wrapper.css('left')).to.equal(originalLeft);
            });
        });
    });

    it('should keep events after interacting with expanded/collapsed sidebar', function () {
        var targetEventsSelector = '.aui-sidebar ul.aui-nav li ul.aui-nav li';
        var clickEvent = sinon.spy();

        // initial state, sidebar is expanded
        sidebar.expand();
        expect(sidebar.isCollapsed()).to.equal(false);

        // counter should be zero because there is not event to be triggered
        $(targetEventsSelector).eq(0).click();
        expect(clickEvent.calledOnce).to.equal(false);
        clickEvent.reset();

        // add event and assure it is working
        $(targetEventsSelector).on('click', clickEvent);
        $(targetEventsSelector).eq(0).click();
        expect(clickEvent.calledOnce).to.equal(true);
        clickEvent.reset();

        sidebar.collapse();
        $(targetEventsSelector).eq(0).click();
        expect(clickEvent.calledOnce).to.equal(true);
        clickEvent.reset();

        // initiate interaction with submenus, which should trigger an inline-dialog to show up
        focus($(sidebar.collapsedTriggersSelector));
        $(targetEventsSelector).eq(0).click();
        expect(clickEvent.calledOnce).to.equal(true);
        clickEvent.reset();

        sidebar.expand();
        $(targetEventsSelector).eq(0).click();
        expect(clickEvent.calledOnce).to.equal(true);
        clickEvent.reset();
    });

    it('sidebar submenus are initialised lazily', function (done) {
        const newSubmenuHtml = `<div class="aui-sidebar-group" tabindex="0" id="test-lazy-submenu-trigger">
            <div class="aui-nav-heading" title="Stuff"><strong>Lazy submenu</strong></div>
            <ul class="aui-nav">
                <li><a href="#lazy-submenu" class="aui-nav-item"><span class="aui-nav-item-label">Lazy submenu</span></a></li>
            </ul>
        </div>`;

        document.querySelector('#content .aui-sidebar .aui-navgroup-inner').innerHTML += newSubmenuHtml;
        afterMutations(function () {
            focus($('#test-lazy-submenu-trigger'));
            afterMutations(function() {
                expect(sidebar.isSubmenuVisible()).to.equal(true);
                done();
            });
        });
    });

    describe('JavaScript events', function () {
        var collapseStart = sinon.spy();
        var collapseEnd = sinon.spy();
        var expandStart = sinon.spy();
        var expandEnd = sinon.spy();

        function resetSpies() {
            collapseStart.reset();
            collapseEnd.reset();
            expandStart.reset();
            expandEnd.reset();
        }

        function expectSpiesToBeCalledOnce() {
            expect(expandStart.calledOnce).to.equal(true);
            expect(expandEnd.calledOnce).to.equal(true);
            expect(collapseStart.calledOnce).to.equal(true);
            expect(collapseEnd.calledOnce).to.equal(true);
        }

        beforeEach(function () {
            sidebar.on('collapse-start', collapseStart);
            sidebar.on('collapse-end', collapseEnd);
            sidebar.on('expand-start', expandStart);
            sidebar.on('expand-end', expandEnd);
        });

        it('should fire when collapsed', function () {
            sidebar.expand();
            resetSpies();

            sidebar.collapse();
            expect(collapseStart.calledOnce).to.equal(true);
            expect(collapseEnd.calledOnce).to.equal(true);
            expect(expandStart.callCount).to.equal(0);
            expect(expandEnd.callCount).to.equal(0);
            expect(collapseStart.calledBefore(collapseEnd)).to.equal(true);
        });

        it('should not fire when already collapsed', function () {
            sidebar.collapse();
            resetSpies();

            sidebar.collapse();
            expect(collapseStart.callCount).to.equal(0);
            expect(collapseEnd.callCount).to.equal(0);
            expect(expandStart.callCount).to.equal(0);
            expect(expandEnd.callCount).to.equal(0);
        });

        it('should fire when expanded', function () {
            sidebar.collapse();
            resetSpies();

            sidebar.expand();
            expect(collapseStart.callCount).to.equal(0);
            expect(collapseEnd.callCount).to.equal(0);
            expect(expandStart.calledOnce).to.equal(true);
            expect(expandEnd.calledOnce).to.equal(true);
            expect(expandStart.calledBefore(expandEnd));
        });

        it('should not fire when already expanded', function () {
            sidebar.expand();
            resetSpies();

            sidebar.expand();
            expect(collapseStart.callCount).to.equal(0);
            expect(collapseEnd.callCount).to.equal(0);
            expect(expandStart.callCount).to.equal(0);
            expect(expandEnd.callCount).to.equal(0);
        });

        it('expand-start should be preventable', function () {
            sidebar.collapse();
            resetSpies();

            sidebar.on('expand-start', function (e) {
                e.preventDefault();
            });

            sidebar.expand();

            expect(expandStart.calledOnce).to.equal(true);
            expect(expandEnd.callCount).to.equal(0);
        });

        it('collapse-start should be preventable', function () {
            sidebar.expand();
            resetSpies();

            sidebar.on('collapse-start', function (e) {
                e.preventDefault();
            });

            sidebar.collapse();

            expect(collapseStart.calledOnce).to.equal(true);
            expect(collapseEnd.callCount).to.equal(0);
        });

        it('should have isResponsive property set to false when expanded/collapsed with click', function () {
            var $toggle = sidebar.$el.find('.aui-sidebar-toggle');

            var numberOfIsResponsiveAssertions = 0;
            sidebar.on('expand-start expand-end collapse-start collapse-end', function (e) {
                expect(e.isResponsive).to.equal(false);
                numberOfIsResponsiveAssertions += 1;
            });
            resetSpies();
            $toggle.click();
            $toggle.click();

            expectSpiesToBeCalledOnce();
            expect(numberOfIsResponsiveAssertions).to.equal(4);
        });

        it('should have isResponsive property set to false when expanded/collapsed with API', function () {
            sidebar.reflow(0, 1024, 768);
            var numberOfIsResponsiveAssertions = 0;
            sidebar.on('expand-start expand-end collapse-start collapse-end', function (e) {
                expect(e.isResponsive).to.equal(false);
                numberOfIsResponsiveAssertions += 1;
            });
            resetSpies();
            sidebar.expand();
            sidebar.collapse();

            expectSpiesToBeCalledOnce();
            expect(numberOfIsResponsiveAssertions).to.equal(4);
        });

        it('should have isResponsive property set to true when expanded/collapsed via viewport size change', function () {
            var numberOfIsResponsiveAssertions = 0;
            sidebar.reflow(0, 1000, 2000);
            sidebar.expand();
            resetSpies();

            sidebar.on('expand-start expand-end collapse-start collapse-end', function (e) {
                expect(e.isResponsive).to.equal(true);
                numberOfIsResponsiveAssertions += 1;
            });

            sidebar.reflow(0, 1000, 1024);
            sidebar.reflow(0, 1000, 2000);

            expectSpiesToBeCalledOnce();
            expect(numberOfIsResponsiveAssertions).to.equal(4);
        });

        describe('can stop listening to events', function () {
            beforeEach(function () {
                sidebar.collapse();
                resetSpies();
            });

            it('with function specified', function () {
                sidebar.expand();
                sidebar.collapse();

                sidebar.off('collapse-start', collapseStart);
                sidebar.off('collapse-end', collapseEnd);
                sidebar.off('expand-start', expandStart);
                sidebar.off('expand-end', expandEnd);

                sidebar.expand();
                sidebar.collapse();

                expectSpiesToBeCalledOnce();
            });

            it('without function specified', function () {
                sidebar.expand();
                sidebar.collapse();

                sidebar.off('collapse-start');
                sidebar.off('collapse-end');
                sidebar.off('expand-start');
                sidebar.off('expand-end');

                sidebar.expand();
                sidebar.collapse();

                expectSpiesToBeCalledOnce();
            });
        });
    });

    describe('javascript API for sidebar.submenus', function () {
        var $submenuTrigger;
        var $noSubmenuTrigger;

        beforeEach(function () {
            sidebar.collapse();
            $submenuTrigger = $('#test-submenu-trigger');
            $noSubmenuTrigger = $('#test-no-submenu-trigger');
        });

        it('.submenu() gets the submenu for a trigger', function () {
            expect(sidebar.submenus.submenu($submenuTrigger).length).to.be.above(0);
        });

        it('.hasSubmenu() returns whether the trigger should have an inline dialog shown in the collapsed state', function () {
            expect(sidebar.submenus.hasSubmenu($submenuTrigger)).to.equal(true);
            expect(sidebar.submenus.hasSubmenu($noSubmenuTrigger)).to.equal(false);
        });

        it('.submenuHeadingHeight() returns a number', function () {
            expect(sidebar.submenus.submenuHeadingHeight()).to.be.a('number');
        });

        it('.isShowing() returns true if a submenu trigger is focused', function () {
            expect(sidebar.submenus.isShowing()).to.equal(false);
            focus($submenuTrigger);
            expect(sidebar.submenus.isShowing()).to.equal(true);
        });

        it('.isShowing() returns true if a submenu trigger is hovered over', function () {
            expect(sidebar.submenus.isShowing()).to.equal(false);
            hover($submenuTrigger);
            expect(sidebar.submenus.isShowing()).to.equal(true);
        });

        it('.show() shows a submenu', function () {
            var e = new CustomEvent('mouseover');

            expect(sidebar.submenus.isShowing()).to.equal(false);
            $submenuTrigger[0].dispatchEvent(e);
            sidebar.submenus.show(e, $submenuTrigger[0]);
            expect(sidebar.submenus.isShowing()).to.equal(true);
        });

        it('triggers aui-sidebar-submenu-before-show event before showing a submenu', function () {
            var e = new CustomEvent('mouseover');
            var eventHandler = sinon.spy();

            $submenuTrigger.on('aui-sidebar-submenu-before-show', eventHandler);

            expect(sidebar.submenus.isShowing()).to.equal(false);
            $submenuTrigger[0].dispatchEvent(e);
            sidebar.submenus.show(e, $submenuTrigger[0]);

            expect(eventHandler.calledOnce).to.equal(true);
            expect(sidebar.submenus.isShowing()).to.equal(true);
        });

        it('aui-sidebar-submenu-before-show event can prevent showing a submenu', function () {
            var e = new CustomEvent('mouseover');
            var inlineDialogEl = null;
            //create a spy
            var eventHandler = sinon.spy(function (e, inlineDialog) {
                e.preventDefault();
                inlineDialogEl = inlineDialog;
            });

            $submenuTrigger.on('aui-sidebar-submenu-before-show', eventHandler);

            expect(sidebar.submenus.isShowing()).to.equal(false);
            $submenuTrigger[0].dispatchEvent(e);
            sidebar.submenus.show(e, $submenuTrigger[0]);

            expect(eventHandler.calledOnce).to.equal(true);
            expect(sidebar.submenus.isShowing()).to.equal(false);
            // make sure second argument is reference to inline dialog
            expect(inlineDialogEl.classList.contains('aui-sidebar-submenu-dialog')).to.equal(true);
        });

        it('.hide() hides currently showing submenus', function () {
            focus($submenuTrigger);
            clock.tick(500);
            expect(sidebar.getVisibleSubmenus().length).to.equal(1);
            sidebar.submenus.hide();
            clock.tick(500);
            expect(sidebar.getVisibleSubmenus().length).to.equal(0);
        });

        it('inline dialog show / hide handler functions exist', function () {
            expect(sidebar.submenus.inlineDialogShowHandler).to.be.a('function');
            expect(sidebar.submenus.inlineDialogHideHandler).to.be.a('function');
        });

        it('inline dialog moveSubmenuToInlineDialog / restoreSubmenu functions exist', function () {
            expect(sidebar.submenus.moveSubmenuToInlineDialog).to.be.a('function');
            expect(sidebar.submenus.restoreSubmenu).to.be.a('function');
        });
    });

    it('should be considered narrow or wide at a certain px width', function () {
        [0, 1024, 1239].forEach(function (width) {
            expect(sidebar.isViewportNarrow(width)).to.equal(true);
        });
        [1240, 1280, 1440].forEach(function (width) {
            expect(sidebar.isViewportNarrow(width)).to.equal(false);
        });
    });

    describe('Accessibility', function () {
        var $submenuTrigger;

        beforeEach(function () {
            $.fx.off = true;
            sidebar.collapse();
            $submenuTrigger = $('#test-submenu-trigger');
        });

        afterEach(function () {
            $.fx.off = false;
        });

        it('submenus trigger can be tabbable', function () {
            var $tabbableItems = sidebar.$el.find(':aui-tabbable:visible');
            expect($tabbableItems.length).to.equal(13);
        });

        describe('Submenu', function () {
            beforeEach(function () {
                // focus on submenu trigger to show the submenu
                focus($submenuTrigger[0]);
                clock.tick(500);
                expect(sidebar.isSubmenuVisible()).to.equal(true);
            });

            it('initializes tabbing', function () {
                // This initializes tabbing for IE 11 and MS Edge
                // No idea why they have a problem, but tabbing once before
                // seems to work.
                // It can't be done in a beforeEach step for some reason.
                // Apologies.
                // If these tests continue to be flaky, think about disabling the section
                pressKey(keyCode.TAB);
            });

            it('can tab to the first submenu item', function () {
                pressKey(keyCode.TAB);
                expect($(document.activeElement).text()).to.equal('Default/fluid');
            });

            it('can shift tab out of the first submenu item', function () {
                // setup
                pressKey(keyCode.TAB);

                // when
                pressKey(keyCode.TAB, {shift: true});
                clock.tick(500);

                // then
                expect(sidebar.isSubmenuVisible()).to.equal(false);
            });

            it('can tab out of the last submenu item', function () {
                // setup
                pressKey(keyCode.TAB);
                focus($(sidebar.getVisibleSubmenus()[0]).find(':aui-tabbable:last'));

                // when
                pressKey(keyCode.TAB);
                clock.tick(500);

                // then
                expect(sidebar.isSubmenuVisible()).to.equal(false);
            });

            it('is not hidden on scroll', function () {
                sidebar.reflow(10);
                clock.tick(500);
                expect(sidebar.isSubmenuVisible()).to.equal(true);
            });
        });
    });

    describe('Tooltip', function () {
        var $noSubmenuTrigger;

        beforeEach(function () {
            sidebar.collapse();
            $noSubmenuTrigger = $('#test-no-submenu-trigger');
            hover($noSubmenuTrigger);
            expect($(sidebar.tooltipSelector).length).to.equal(1);
        });

        it('is hidden on scroll', function () {
            sidebar.reflow(10);
            clock.tick(500);
            expect($(sidebar.tooltipSelector).length).to.equal(0);
        });
    });

    describe('Expandable submenu', function () {
        var $submenuTrigger;
        const getLiFromSubmenu = $submenuTrigger => {
            focus($submenuTrigger);
            clock.tick(500);
            const submenu = sidebar.getVisibleSubmenus()[0];
            return submenu.querySelector('li');
        };

        beforeEach(function () {
            sidebar.collapse();
            $submenuTrigger = $('#test-expandable-submenu-trigger');
        });

        it('should already be in expand mode when the inline dialog is shown', function () {
            focus($submenuTrigger);
            clock.tick(500);
            var visibleSubmenus = sidebar.getVisibleSubmenus();

            expect(visibleSubmenus.length).to.equal(1);
            expect($(visibleSubmenus[0]).find('li:visible').length).to.equal(5);
        });

        it('should not reopen submenu if nested inline dialog fired aui-layer-show event', function () {
            const li = getLiFromSubmenu($submenuTrigger);
            li.innerHTML = '<aui-inline-dialog id="start-open"></aui-inline-dialog>';

            const eventHandler = sinon.spy();
            $submenuTrigger.on('aui-sidebar-submenu-before-show', eventHandler);
            li.querySelector('#start-open').setAttribute('open', '');

            clock.tick(500);

            expect(eventHandler.calledOnce).to.equal(false);
        });

        it('should not show submenu if other target than submenu inline dialog fired aui-layer-show event', function () {
            const li = getLiFromSubmenu($submenuTrigger);

            li.innerHTML = '<aui-dropdown-menu id="nested-dropdown"></aui-dropdown-menu>';

            const eventHandler = sinon.spy();
            $submenuTrigger.on('aui-sidebar-submenu-before-show', eventHandler);
            dispatch('aui-layer-show', li.querySelector('#nested-dropdown'));

            expect(eventHandler.calledOnce).to.equal(false);
        });
    });
});

describe('Sidebar with no submenus', function () {
    var sidebar;
    var clock;

    beforeEach(function () {
        $('#test-fixture').html(sidebarHtmlNoSubmenus);

        sidebar = new SidebarFn($('.aui-sidebar'));

        clock = sinon.useFakeTimers();
    });

    afterEach(function () {
        clock.restore();
    });

    it('Sidebar.isSubmenuVisible() returns false', function () {
        expect(sidebar.isSubmenuVisible()).to.equal(false);
    });

    it('Hovering over an item with no inner .aui-nav means that no submenu is shown', function () {
        focus(document.getElementById('test-sidebar-group-no-submenu'));
        clock.tick(500);
        expect(sidebar.isSubmenuVisible()).to.equal(false);
    });
});
