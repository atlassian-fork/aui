require([
    'jquery',
    'auitest/restfultable-server'
], function (
    $,
    server
) {
    /* eslint-disable no-undef */
    const auiFlag = AJS.flag;
    const RestfulTable = AJS.RestfulTable;
    /* eslint-enable no-undef */

    const EditGroupView = RestfulTable.CustomEditView.extend({
        render(self) {
            const $select = $(`
                <select name='group' class='select'>
                    <option value='Friends'>Friends</option>
                    <option value='Family'>Family</option>
                    <option value='Work'>Work</option>
                </select>`);

            $select.val(self.value); // select currently selected
            return $select;
        }
    });

    const NameReadView = RestfulTable.CustomReadView.extend({
        render: (self) => $('<strong />').text(self.value)
    });

    const CheckboxEditView = RestfulTable.CustomEditView.extend({
        render: (self) => $(`
                <input type='checkbox' class='ajs-restfultable-input-${self.name}' />
                <input type='hidden' name='${self.name}'/>
        `)
    });

    const DummyReadView = RestfulTable.CustomReadView.extend({
        render: () => $('<strong />').text('Blah')
    });

    // DOM ready
    $(function () {
        Object.keys(RestfulTable.Events).forEach(function(eventKey) {
            const eventName = RestfulTable.Events[eventKey];
            $(document).one(eventName, function() {
                auiFlag({
                    id: eventName,
                    type: eventName === 'SERVER_ERROR' ? 'error' : 'info',
                    body: eventName + ' fired on AJS. Used for testing AJS events.',
                    close: 'auto',
                });
            });

            $(document).on(eventName, function(e, ...args) {
                console.log(eventName, e.target, args);
                $('#message-area').append(`
<p>
    <time>${Date.now()}</time>
    <strong>${eventName}</strong>
</p>
                `);
            });
        });

        const baseConfig = {
            autofocus: true, // auto focus first field of create row
            columns: [
                {id: 'name', header: 'Name', readView: NameReadView, inputAriaLabel: 'name field'}, // id is the mapping of the rest property to render
                {id: 'group', header: 'Group', editView: EditGroupView}, // header is the text in the <th>
                {id: 'number', header: 'Number'},
                {id: 'checkbox', header: 'Checkbox', readView: DummyReadView, editView: CheckboxEditView}
            ],
            resources: {
                all: server.url, // resource to get all contacts
                self: server.url // resource to get single contact url/{id}
            },
            noEntriesMsg: 'You have no contacts. Loner!', // message to be displayed when table is empty
            allowReorder: true, // drag and drop reordering
            fieldFocusSelector: name => `:input[type!=hidden][name="${name}"], #${name}, .ajs-restfultable-input-${name}`
        };

        new RestfulTable(Object.assign({}, baseConfig, {
            el: $('#contacts-table')
        }));

        // duplicate of the first table but with the addPosition: 'bottom' option applied.
        new RestfulTable(Object.assign({}, baseConfig, {
            el: $('#contacts-table-addPositionBottom'), // <table>
            addPosition: 'bottom'
        }));
    });

});
