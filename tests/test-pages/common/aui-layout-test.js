require(['jquery'], function($){

    function setClassToggles(options) {
        var theTarget = $(options.target);
        var classes = (theTarget.attr('class') || '').split(/\s+/);
        var theClass;
        var triggerWrap = $(options.triggerWrap);
        var triggers = [];
        var trigger;

        $.each(classes, function(i) {
            trigger = $('<button></button>').attr('id',classes[i]).text(classes[i]);
            triggerWrap.append(trigger);
            triggers.push(trigger);
        });

        function updateMessage(trigger,target,value) {
            if (target.hasClass(value)) {
                trigger.text('Turn ' + value + ' off');
            } else {
                trigger.text('Turn ' + value + ' on');
            }
        }

        $.each(triggers, function() {
            // set up button text
            updateMessage($(this), theTarget, $(this).attr('id'));

            // toggle class and text on click
            $(this).click(function() {
                theClass = $(this).attr('id');
                theTarget.toggleClass(theClass);
                updateMessage($(this),theTarget,theClass);
            })
        });
    }

    $(document).ready(function() {
        setClassToggles({ 'triggerWrap': '#toggles', 'target': 'body' });
    });

});
